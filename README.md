## JUPYTER

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/gdolle%2Fjupyterlab-demo/HEAD?filepath=rlang.ipynb)

## JUPYTERLAB

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/gdolle%2Fjupyterlab-demo/HEAD?filepath=rlang.ipynb&urlpath=lab)

## RSTUDIO

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/gdolle%2Fjupyterlab-demo/HEAD?filepath=rlang.ipynb&urlpath=rstudio)
